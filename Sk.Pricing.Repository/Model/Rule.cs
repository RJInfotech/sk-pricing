﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Sk.Pricing.Shared.Enums;

namespace Sk.Pricing.Repository.Model
{
    public class Rule : IIdentity<string>
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("offers")]
        public List<Offer> Offers { get; set; }
    }
}