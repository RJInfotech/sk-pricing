﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sk.Pricing.Repository.Model
{
    public class JsonData<T>
    {
        [JsonProperty("rows")]
        public List<T> Rows { get; set; }
    }
}
