using Newtonsoft.Json;

namespace Sk.Pricing.Repository.Model
{
    public class Product : IIdentity<string>
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
    }
}