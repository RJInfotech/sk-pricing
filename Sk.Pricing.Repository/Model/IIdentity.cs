﻿namespace Sk.Pricing.Repository.Model
{
    public interface IIdentity<T>
    {
        T Id { get; set; }
    }
}
