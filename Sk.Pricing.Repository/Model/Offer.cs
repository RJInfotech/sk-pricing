using Newtonsoft.Json;
using Sk.Pricing.Shared.Enums;

namespace Sk.Pricing.Repository.Model
{
    public class Offer
    {
        [JsonProperty("product_id")]
        public string ProductId { get; set; }
        [JsonProperty("offer_type")]
        public OfferType OfferType { get; set; }
        [JsonProperty("purchase")]
        public int? Purchase { get; set; }
        [JsonProperty("pay")]
        public int? Pay { get; set; }
        [JsonProperty("value")]
        public decimal? Value { get; set; }
        [JsonProperty("min_purchase")]
        public int? MinPurchase { get; set; }
    }
}