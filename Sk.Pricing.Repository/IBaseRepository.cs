﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Sk.Pricing.Repository.Model;

namespace Sk.Pricing.Repository
{
    public interface IBaseRepository<T>
        where T : class
    {
        IEnumerable<T> GetAll();

        T FirstOrDefault(Func<T,bool> predicate);
    }

}
