﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sk.Pricing.BusinessLogic.Exceptions
{
    public class ProductNotFoundException : KeyNotFoundException
    {
        public ProductNotFoundException(string productId)
            : base($"{productId} not found")
        {
            
        }
    }
}
