﻿using System.Collections.Generic;

namespace Sk.Pricing.BusinessLogic.Exceptions
{
    public class RuleNotFoundException : KeyNotFoundException
    {
        public RuleNotFoundException(string customer)
            : base($"Customer type{customer} not found")
        {

        }
    }
}