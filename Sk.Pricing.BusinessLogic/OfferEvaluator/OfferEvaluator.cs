﻿using System.Linq;
using Sk.Pricing.Repository.Model;
using Sk.Pricing.Shared.Enums;
using Sk.Pricing.Shared.Models;

namespace Sk.Pricing.BusinessLogic.OfferEvaluator
{
    public abstract class OfferEvaluator : IOfferEvaluator
    {
        private readonly OfferType _offerType;

        protected OfferEvaluator(OfferType offerType)
        {
            _offerType = offerType;
        }

        public bool TryEvaluateOffers(Rule rule, EvaluationRequest request)
        {
            var applicableOffers = rule.Offers.Where(o => o.OfferType == _offerType).ToList();
            int offersApplied = 0;
            foreach (var offer in applicableOffers)
            {
                if (TryEvaluateOffers(offer, request))
                {
                    offersApplied++;
                }
            }

            return offersApplied > 0;
        }

        protected abstract bool TryEvaluateOffers(Offer offer, EvaluationRequest request);
    }
}