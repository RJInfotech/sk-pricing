using System.Collections.Generic;
using System.Linq;
using Sk.Pricing.Repository.Model;
using Sk.Pricing.Shared.Enums;
using Sk.Pricing.Shared.Models;

namespace Sk.Pricing.BusinessLogic.OfferEvaluator
{
    public class DiscountEvaluator : OfferEvaluator
    {
        public DiscountEvaluator() : base(OfferType.Discount)
        {
        }

        protected override bool TryEvaluateOffers(Offer offer, EvaluationRequest request)
        {
            var billableProduct = request.BillableProducts.FirstOrDefault(bp => bp.ProductId == offer.ProductId);
            if (billableProduct != null && offer.MinPurchase.HasValue && offer.Value.HasValue && billableProduct.Count >= offer.MinPurchase.Value)
            {
                billableProduct.Cost = offer.Value.Value;
                return true;
            }
            return false;
        }
    }
}