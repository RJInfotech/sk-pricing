﻿using System.Collections.Generic;
using Sk.Pricing.Repository.Model;
using Sk.Pricing.Shared.Models;

namespace Sk.Pricing.BusinessLogic.OfferEvaluator
{
    public interface IOfferEvaluator
    {
        bool TryEvaluateOffers(Rule rule, EvaluationRequest request);
    }
}