﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sk.Pricing.Repository;
using Sk.Pricing.Repository.Model;
using Sk.Pricing.Shared.Enums;
using Sk.Pricing.Shared.Models;

namespace Sk.Pricing.BusinessLogic.OfferEvaluator
{
    public class X4YOfferEvaluator : OfferEvaluator
    {
        public X4YOfferEvaluator() : base(OfferType.X4Y)
        {
        }

        protected override bool TryEvaluateOffers(Offer offer, EvaluationRequest request)
        {
            var billableProduct = request.BillableProducts.FirstOrDefault(bp => bp.ProductId == offer.ProductId);
            if (billableProduct != null && offer.Purchase.HasValue && offer.Pay.HasValue && billableProduct.Count >= offer.Purchase.Value)
            {
                int billableSet = (billableProduct.Count / offer.Purchase.Value);
                billableProduct.Count = billableProduct.Count - (billableSet * offer.Purchase.Value) +
                                        (billableSet * offer.Pay.Value);
                return true;
            }

            return false;
        }
    }
}
