﻿using Sk.Pricing.Shared.Models;

namespace Sk.Pricing.BusinessLogic
{
    public interface IPricingEngine
    {
        EvaluationResponse Evaluate(PurchasingProduct purchasingProduct);
    }
}