﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sk.Pricing.BusinessLogic.Exceptions;
using Sk.Pricing.BusinessLogic.OfferEvaluator;
using Sk.Pricing.Repository;
using Sk.Pricing.Repository.Model;
using Sk.Pricing.Shared.Enums;
using Sk.Pricing.Shared.Models;
using Sk.Pricing.Shared.Utils;

namespace Sk.Pricing.BusinessLogic
{
    public class PricingEngine : IPricingEngine
    {
        private readonly IBaseRepository<Product> _productRepository;
        private readonly IBaseRepository<Rule> _ruleRepository;
        private readonly IOfferEvaluator[] _offerEvaluators;

        private const string DefaultRuleId = "default";

        public PricingEngine(IBaseRepository<Product> productRepository, IBaseRepository<Rule> ruleRepository, IOfferEvaluator[] offerEvaluators)
        {
            _productRepository = productRepository;
            _ruleRepository = ruleRepository;
            _offerEvaluators = offerEvaluators;
        }

        public EvaluationResponse Evaluate(PurchasingProduct purchasingProduct)
        {
            Guard.ArgumentNotNull(purchasingProduct);
            Guard.ArgumentNotNull(purchasingProduct.Products);

            Dictionary<string, decimal> productRates = _productRepository.GetAll().ToDictionary(p => p.Id, p => p.Price);
            var applicableRule = _ruleRepository.FirstOrDefault(r => r.Id == purchasingProduct.CustomerType);

            if (applicableRule == null)
            {
                throw new RuleNotFoundException(purchasingProduct.CustomerType);
            }

            var request = new EvaluationRequest(purchasingProduct.Products, applicableRule.Id, productRates);
            var evaluationResults = _offerEvaluators.Select(ev => ev.TryEvaluateOffers(applicableRule, request)).ToList();
            
            if (!evaluationResults.Any(er => er))
            {
                request = new EvaluationRequest(purchasingProduct.Products, DefaultRuleId, productRates);
            }

            return request.Response;
        }
    }
}
