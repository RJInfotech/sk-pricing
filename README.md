# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Endpoint ###

* POST /api/checkout

**Body**

```
#!json

{
	"customer" : "apple",
	"products": [ "classic" , "standout"	,"premium"	]
}
```


For. eg.

![Capture-sample-request.PNG](https://bitbucket.org/repo/Egg4yk8/images/937119168-Capture-sample-request.PNG)

Json Data for Product list Refer. **Sk.Pricing.Repository.Json -> Data -> Product.Json**
```
#!json
{
  "rows": [
    {
      "id": "classic",
      "name": "Classic Ad",
      "price": 269.99
    },
    {
      "id": "standout",
      "name": "Standout Ad",
      "price": 322.99
    },
    {
      "id": "premium",
      "name": "Premium Ad",
      "price": 394.99
    }
  ] 
}

```

Json data for price rule list: Refer. **Sk.Pricing.Repository.Json -> Data -> rule.Json**
```
#!json

{
  "rows": [
    {
      "id": "unilever",
      "name": "Unilever",
      "offers": [
        {
          "product_id": "classic",
          "offer_type": "X4Y",
          "purchase": 3,
          "pay": 2
        }
      ]
    },
    {
      "id": "apple",
      "name": "Apple",
      "offers": [
        {
          "product_id": "standout",
          "offer_type": "Discount",
          "min_purchase": 1,
          "value": 299.99
        }
      ]
    },
    {
      "id": "nike",
      "name": "Nike",
      "offers": [
        {
          "product_id": "premium",
          "offer_type": "Discount",
          "min_purchase": 4,
          "value": 379.99
        }
      ]
    },
    {
      "id": "ford",
      "name": "Ford",
      "offers": [
        {
          "product_id": "classic",
          "offer_type": "X4Y",
          "purchase": 5,
          "pay": 4
        },
        {
          "product_id": "standard",
          "offer_type": "Discount",
          "min_purchase": 1,
          "value": 309.99
        },
        {
          "product_id": "premium",
          "offer_type": "Discount",
          "min_purchase": 3,
          "value": 389.99
        }
      ]
    }
  ] 

}

```



### Who do I talk to? ###
* Ramji Piramanayagam