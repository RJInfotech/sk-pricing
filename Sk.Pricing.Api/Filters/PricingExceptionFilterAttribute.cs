﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using FluentValidation;
using Newtonsoft.Json;
using Sk.Pricing.BusinessLogic.Exceptions;

namespace Sk.Pricing.Api.Filters
{
    public class PricingExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is KeyNotFoundException)
            {
                context.Response = GetResponse(HttpStatusCode.NotFound, context.Exception.Message);
            } else if (context.Exception is ValidationException)
            {
                context.Response = GetResponse(HttpStatusCode.BadRequest,
                    new {Errors = (context.Exception as ValidationException).Errors});

            }
        }

        private HttpResponseMessage GetResponse(HttpStatusCode statusCode, string content)
        {
            return new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(content)
            };
        }

        private HttpResponseMessage GetResponse(HttpStatusCode statusCode, object obj)
        {
            return GetResponse(statusCode, JsonConvert.SerializeObject(obj));
        }
    }
}