using System.Web.Http;
using FluentValidation;

namespace Sk.Pricing.Api.Controllers
{
    public class BaseApiController : ApiController
    {
        protected void ValidateRequest<TValidator, T>(TValidator validator, T requestObject)
            where TValidator : AbstractValidator<T>
        {
            validator.ValidateAndThrow(requestObject);
        }
    }
}