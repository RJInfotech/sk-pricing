﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using Sk.Pricing.BusinessLogic;
using Sk.Pricing.Shared.Models;
using Sk.Pricing.Shared.ModelValidators;

namespace Sk.Pricing.Api.Controllers
{
    public class CheckoutController : BaseApiController
    {
        private readonly IPricingEngine _pricingEngine;
        public CheckoutController(IPricingEngine pricingEngine)
        {
            _pricingEngine = pricingEngine;
        }

        [HttpPost]
        [ResponseType(typeof(EvaluationResponse))]
        public IHttpActionResult Post([FromBody] PurchasingProduct purchasingProduct)
        {
            ValidateRequest(new PurchasingProductValidator(), purchasingProduct);

            var response = _pricingEngine.Evaluate(purchasingProduct);
            return Ok(response);
        }
    }
}
