﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Sk.Pricing.BusinessLogic.Ioc;
using Sk.Pricing.Repository.Json.Ioc;

namespace Sk.Pricing.Api.Ioc
{
    public static class AutofacHelper
    {
        public static void RegisterWebApiTypes(this ContainerBuilder builder, HttpConfiguration httpConfiguration)
        {
            var assembly = typeof(WebApiApplication).Assembly;
            builder.RegisterApiControllers(assembly);
            builder.RegisterWebApiFilterProvider(httpConfiguration);
        }

        public static void RegisterWebAppCustomModules(this ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules<BlModule>(typeof(BlModule).Assembly);
            builder.RegisterAssemblyModules<JsonRepoModule>(typeof(JsonRepoModule).Assembly);
        }
    }
}