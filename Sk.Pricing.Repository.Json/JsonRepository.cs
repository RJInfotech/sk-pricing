﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sk.Pricing.Repository.Model;

namespace Sk.Pricing.Repository.Json
{
    public class JsonRepository<T> : IBaseRepository<T>
        where T : class
    {
        private readonly ICollection<T> Entities;

        public JsonRepository()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"Sk.Pricing.Repository.Json.Data.{typeof(T).Name.ToLower()}.json";
            string fileContent = string.Empty;

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                fileContent = reader.ReadToEnd();
            }
            //string path = System.IO.Path.GetDirectoryName(assembly.Location);
            //var filePath = Path.Combine(path, "Data", $"{typeof(T).Name}.json");
            //var fileContent = File.ReadAllText(filePath);
            JsonData<T> data = JsonConvert.DeserializeObject<JsonData<T>>(fileContent);
            Entities = data.Rows;
        }

        public IEnumerable<T> GetAll()
        {
            return Entities;
        }

        public T FirstOrDefault(Func<T,bool>  predicate)
        {
            return Entities.FirstOrDefault(predicate);
        }
    }
}
