﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace Sk.Pricing.Repository.Json.Ioc
{
    public class JsonRepoModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly).AsImplementedInterfaces();
            builder.RegisterGeneric(typeof(JsonRepository<>)).As(typeof(IBaseRepository<>));
            base.Load(builder);
        }
    }
}
