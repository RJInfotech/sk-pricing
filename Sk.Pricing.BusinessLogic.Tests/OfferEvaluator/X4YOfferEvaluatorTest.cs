﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sk.Pricing.BusinessLogic.OfferEvaluator;
using Sk.Pricing.Repository.Model;
using Sk.Pricing.Shared.Enums;
using Sk.Pricing.Shared.Models;

namespace Sk.Pricing.BusinessLogic.Tests.OfferEvaluator
{
    [TestClass]
    public class X4YOfferEvaluatorTest
    {
        private X4YOfferEvaluator _subject;
        private EvaluationRequest _request;

        private readonly Dictionary<string, decimal> _pricelist = new Dictionary<string, decimal>()
        {
            {"p1", 10M},
            {"p2", 50M},
            {"p3", 100M}
        };

        [TestInitialize]
        public void Initialize()
        {
            _subject = new X4YOfferEvaluator();
        }


        [TestMethod]
        public void TryEvaluateOffers_3For2_None()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.X4Y,
                        ProductId = "p1",
                        Purchase = 3,
                        Pay = 2
                    }
                }
            }, _request);

            Assert.AreEqual(response, false);
        }

        [TestMethod]
        public void TryEvaluateOffers_3For2_Single()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.X4Y,
                        ProductId = "p1",
                        Purchase = 3,
                        Pay = 2
                    }
                }
            }, _request);

            Assert.AreEqual(response, true);
            Assert.AreEqual(_request.Response.Total, 70M);
        }

        [TestMethod]
        public void TryEvaluateOffers_3For2_Multiple()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1", "p1", "p1", "p1" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.X4Y,
                        ProductId = "p1",
                        Purchase = 3,
                        Pay = 2
                    }
                }
            }, _request);

            Assert.AreEqual(true, response);
            Assert.AreEqual(_request.Response.Total, 90M);
        }
        
        [TestMethod]
        public void TryEvaluateOffers_MultiProduct_SomeSatisfied()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1", "p2", "p2" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.X4Y,
                        ProductId = "p1",
                        Purchase = 3,
                        Pay = 2
                    },

                    new Offer()
                    {
                        OfferType = OfferType.X4Y,
                        ProductId = "p2",
                        Purchase = 4,
                        Pay = 3
                    }
                }
            }, _request);

            Assert.AreEqual(true, response);
            Assert.AreEqual(_request.Response.Total, 170M);
        }


        [TestMethod]
        public void TryEvaluateOffers_MultiProduct_AllSatisfied()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1", "p2", "p2" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.X4Y,
                        ProductId = "p1",
                        Purchase = 3,
                        Pay = 2
                    },

                    new Offer()
                    {
                        OfferType = OfferType.X4Y,
                        ProductId = "p2",
                        Purchase = 2,
                        Pay = 1
                    }
                }
            }, _request);

            Assert.AreEqual(true, response);
            Assert.AreEqual(_request.Response.Total, 120M);
        }

        [TestMethod]
        public void TryEvaluateOffers_MultiProduct_NoneSatisfied()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1", "p2", "p2" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.X4Y,
                        ProductId = "p1",
                        Purchase = 5,
                        Pay = 4
                    },

                    new Offer()
                    {
                        OfferType = OfferType.X4Y,
                        ProductId = "p3",
                        Purchase = 2,
                        Pay = 1
                    }
                }
            }, _request);

            Assert.AreEqual(false, response);
        }
    }
}
