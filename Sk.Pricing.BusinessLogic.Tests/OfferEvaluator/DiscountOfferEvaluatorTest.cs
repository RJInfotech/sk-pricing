﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sk.Pricing.BusinessLogic.OfferEvaluator;
using Sk.Pricing.Repository.Model;
using Sk.Pricing.Shared.Enums;
using Sk.Pricing.Shared.Models;

namespace Sk.Pricing.BusinessLogic.Tests.OfferEvaluator
{
    [TestClass]
    public class DiscountOfferEvaluatorTest
    {
        private DiscountEvaluator _subject;
        private EvaluationRequest _request;

        private readonly Dictionary<string, decimal> _pricelist = new Dictionary<string, decimal>()
        {
            {"p1", 10M},
            {"p2", 50M},
            {"p3", 100M}
        };

        [TestInitialize]
        public void Initialize()
        {
            _subject = new DiscountEvaluator();
        }


        [TestMethod]
        public void TryEvaluateOffers_Discount_None()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p3",
                        MinPurchase = 3,
                        Value = 75
                    }
                }
            }, _request);

            Assert.AreEqual(response, false);
            Assert.AreEqual(_request.Response.Total, 60M);
        }

        [TestMethod]
        public void TryEvaluateOffers_Discount_Single()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p3", "p1", "p1" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p3",
                        MinPurchase = 1,
                        Value = 75
                    }
                }
            }, _request);

            Assert.AreEqual(response, true);
            Assert.AreEqual(_request.Response.Total, 105M);
        }

        [TestMethod]
        public void TryEvaluateOffers_Discount_MinPurchase()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1", "p1", "p1", "p1" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p1",
                        MinPurchase = 4,
                        Value = 5
                    }
                }
            }, _request);

            Assert.AreEqual(true, response);
            Assert.AreEqual(_request.Response.Total, 80M);
        }

        [TestMethod]
        public void TryEvaluateOffers_Discount_MinPurchaseNotMet()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p1",
                        MinPurchase = 4,
                        Value = 5
                    }
                }
            }, _request);

            Assert.AreEqual(false, response);
            Assert.AreEqual(_request.Response.Total, 80M);
        }

        [TestMethod]
        public void TryEvaluateOffers_Discount_MultiProduct_SomeMet()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p1",
                        MinPurchase = 3,
                        Value = 5
                    },
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p2",
                        MinPurchase = 2,
                        Value = 25
                    }
                }
            }, _request);

            Assert.AreEqual(true, response);
            Assert.AreEqual(65, _request.Response.Total);
        }

        [TestMethod]
        public void TryEvaluateOffers_Discount_MultiProduct_AllMet()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p1",
                        MinPurchase = 3,
                        Value = 5
                    },
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p2",
                        MinPurchase = 1,
                        Value = 25
                    }
                }
            }, _request);

            Assert.AreEqual(true, response);
            Assert.AreEqual(40, _request.Response.Total);
        }

        [TestMethod]
        public void TryEvaluateOffers_Discount_MultiProduct_NoneMet()
        {
            _request = new EvaluationRequest(new List<string>() { "p1", "p2", "p1", "p1" }, "r1", _pricelist);
            var response = _subject.TryEvaluateOffers(new Rule()
            {
                Id = "r1",
                Name = "Rule 1",
                
                Offers = new List<Offer>()
                {
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p1",
                        MinPurchase = 4,
                        Value = 5
                    },
                    new Offer()
                    {
                        OfferType = OfferType.Discount,
                        ProductId = "p3",
                        MinPurchase = 1,
                        Value = 50
                    }
                }
            }, _request);

            Assert.AreEqual(false, response);
        }
    }
}
