﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Sk.Pricing.BusinessLogic.OfferEvaluator;
using Sk.Pricing.Repository;
using Sk.Pricing.Repository.Model;
using Sk.Pricing.Shared.Enums;
using Sk.Pricing.Shared.Models;

namespace Sk.Pricing.BusinessLogic.Tests
{
    [TestClass]
    public class PricingEngineTest
    {
        private PricingEngine _subject;
        private IBaseRepository<Product> _productRepository;
        private IBaseRepository<Rule> _pricingRuleRepository;
        private IOfferEvaluator[] evaluators = new IOfferEvaluator[] {new X4YOfferEvaluator(), new DiscountEvaluator()};

        private static class Products
        {
            public const string Classic = "classic";
            public const string StandOut = "standout";
            public const string Premium = "premium";
        }

        private readonly Rule Classic_X4Y = new Rule()
        {
            Id = "classicX4Y",
            Name = "X4Y offer classic",
            
            Offers = new List<Offer>()
            {
                new Offer()
                {
                    Purchase = 3,
                    Pay = 2,
                    ProductId = Products.Classic,
                    OfferType = OfferType.X4Y
                }
            }
        };


        private readonly Rule Standout_Discount = new Rule()
        {
            Id = "standoutdiscount",
            Name = "Discount offer standout",
            
            Offers = new List<Offer>()
            {
                new Offer()
                {
                    Value = 25M,
                    MinPurchase = 1,
                    ProductId = Products.StandOut,
                    OfferType = OfferType.Discount
                }
            }
        };

        private readonly Rule Premium_Discount_MinPurchase = new Rule()
        {
            Id = "premiumdiscount",
            Name = "Discount offer premium",
            
            Offers = new List<Offer>()
            {
                new Offer()
                {
                    Value = 75M,
                    MinPurchase = 4,
                    ProductId = Products.Premium,
                    OfferType = OfferType.Discount
                }
            }
        };

        private readonly Rule Discount_X4Y = new Rule()
        {
            Id = "disx4y",
            Name = "Discount and X4Y offer",
            
            Offers = new List<Offer>()
            {
                new Offer()
                {
                    Value = 75M,
                    MinPurchase = 2,
                    ProductId = Products.Premium,
                    OfferType = OfferType.Discount
                },

                new Offer()
                {
                    Purchase = 2,
                    Pay = 1,
                    ProductId = Products.Classic,
                    OfferType = OfferType.X4Y
                }
            }
        };
        
        private readonly Rule Multi_Discount = new Rule()
        {
            Id = "multidis",
            Name = "Multi discount offer",
            
            Offers = new List<Offer>()
            {
                new Offer()
                {
                    Value = 25M,
                    MinPurchase = 2,
                    ProductId = Products.StandOut,
                    OfferType = OfferType.Discount
                },

                new Offer()
                {
                    Value = 5M,
                    MinPurchase = 1,
                    ProductId = Products.Classic,
                    OfferType = OfferType.Discount
                }
            }
        };


        private readonly Rule Multi_X4Y = new Rule()
        {
            Id = "multidis",
            Name = "Multi discount offer",

            Offers = new List<Offer>()
            {
                new Offer()
                {
                    Purchase = 3,
                    Pay = 2,
                    ProductId = Products.StandOut,
                    OfferType = OfferType.X4Y
                },

                new Offer()
                {
                    Purchase = 2,
                    Pay = 1,
                    ProductId = Products.Classic,
                    OfferType = OfferType.X4Y
                }
            }
        };


        private readonly Rule Multi_Discount_Multi_X4Y = new Rule()
        {

            Id = "multidismultix4y",
            Name = "Multi discount and multi x4y offer",

            Offers = new List<Offer>()
            {

                new Offer()
                {
                    Value = 25M,
                    MinPurchase = 2,
                    ProductId = Products.StandOut,
                    OfferType = OfferType.Discount
                },

                new Offer()
                {
                    Value = 75M,
                    MinPurchase = 1,
                    ProductId = Products.Premium,
                    OfferType = OfferType.Discount
                },
                new Offer()
                {
                    Purchase = 3,
                    Pay = 2,
                    ProductId = Products.StandOut,
                    OfferType = OfferType.X4Y
                },

                new Offer()
                {
                    Purchase = 2,
                    Pay = 1,
                    ProductId = Products.Classic,
                    OfferType = OfferType.X4Y
                }
            }
        };
 
        [TestInitialize]
        public void Initialize()
        {
            _productRepository = Substitute.For<IBaseRepository<Product>>();
            _pricingRuleRepository = Substitute.For<IBaseRepository<Rule>>();
            _subject = new PricingEngine(_productRepository, _pricingRuleRepository, evaluators);
            _productRepository.GetAll().Returns(new List<Product>()
            {
                new Product()
                {
                    Id = Products.Classic,
                    Name = "Classic",
                    Price = 10M
                },
                new Product()
                {
                    Id = Products.StandOut,
                    Name = "StandOut",
                    Price = 50M
                },
                new Product()
                {
                    Id = Products.Premium,
                    Name = "premium",
                    Price = 100M
                }
            });

        }

        [TestMethod]
        public void Evaluate_Classic_X4Y()
        {
            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Classic_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Classic_X4Y.Id,
                Products = new List<string>()
                {
                    "classic",
                    "classic",
                    "standout",
                    "classic"
                }
            });

            AssertResponse(response, 70M);
        }

        [TestMethod]
        public void Evaluate_Classic_X4Y_Multi()
        {
            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Classic_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Classic_X4Y.Id,
                Products = new List<string>()
                {
                    "classic",
                    "classic",
                    "standout",
                    "classic",
                    "classic",
                    "classic",
                    "classic",
                    "premium"
                }
            });

            AssertResponse(response, 190M);
        }

        [TestMethod]
        public void Evaluate_Standard_Discount()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Standout_Discount);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Standout_Discount.Id,
                Products = new List<string>()
                {
                    "classic",
                    "standout",
                    "standout",
                    "classic"
                }
            });

            AssertResponse(response, 70M);
        }

        [TestMethod]
        public void Evaluate_Premium_Discount_WithMinPurchase()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Premium_Discount_MinPurchase);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Premium_Discount_MinPurchase.Id,
                Products = new List<string>()
                {
                    "classic",
                    "standout",
                    "premium",
                    "classic",
                    "premium",
                    "premium",
                    "premium"
                }
            });

            AssertResponse(response, 370M);
        }

        [TestMethod]
        public void Evaluate_Discount_X4Y_OnlyDiscount()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Discount_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Discount_X4Y.Id,
                Products = new List<string>()
                {
                    "premium",
                    "premium",
                    "premium",
                    "premium"
                }
            });

            AssertResponse(response, 300M);
        }

        [TestMethod]
        public void Evaluate_Discount_X4Y_OnlyX4Y()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Discount_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Discount_X4Y.Id,
                Products = new List<string>()
                {
                    "classic",
                    "classic",
                    "classic",
                    "classic"
                }
            });

            AssertResponse(response, 20M);
        }


        [TestMethod]
        public void Evaluate_Discount_X4Y_BothDiscountAndX4Y()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Discount_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Discount_X4Y.Id,
                Products = new List<string>()
                {
                    "classic",
                    "classic",
                    "premium",
                    "premium"
                }
            });
            AssertResponse(response, 160M);
        }

        [TestMethod]
        public void Evaluate_Multi_Discount_SomeDiscount()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Multi_Discount);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Multi_Discount.Id,
                Products = new List<string>()
                {
                    "standout",
                    "standout",
                    "premium",
                    "premium"
                }
            });

            AssertResponse(response, 250M);
        }

        [TestMethod]
        public void Evaluate_Multi_Discount_All()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Multi_Discount);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Multi_Discount.Id,
                Products = new List<string>()
                {
                    "standout",
                    "standout",
                    "classic",
                    "classic"
                }
            });

            AssertResponse(response, 60M);
        }


        [TestMethod]
        public void Evaluate_Multi_X4Y_SomeX4Y()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Multi_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Multi_X4Y.Id,
                Products = new List<string>()
                {
                    "standout",
                    "standout",
                    "standout",
                    "premium"
                }
            });

            AssertResponse(response, 200M);
        }

        [TestMethod]
        public void Evaluate_Multi_X4Y_All()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Multi_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Multi_X4Y.Id,
                Products = new List<string>()
                {
                    "standout",
                    "standout",
                    "standout",
                    "classic",
                    "classic"
                }
            });

            AssertResponse(response, 110M);
        }


        [TestMethod]
        public void Evaluate_Multi_Discount_Multi_X4Y_SomeX4Y()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Multi_Discount_Multi_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Multi_Discount_Multi_X4Y.Id,
                Products = new List<string>()
                {
                    "standout",
                    "standout",
                    "standout"
                }
            });

            AssertResponse(response, 50M);
        }

        [TestMethod]
        public void Evaluate_Multi_Discount_Multi_X4Y_AllX4Y()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Multi_Discount_Multi_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Multi_Discount_Multi_X4Y.Id,
                Products = new List<string>()
                {
                    "standout",
                    "standout",
                    "standout",
                    "classic",
                    "classic"
                }
            });

            AssertResponse(response, 60M);
        }


        [TestMethod]
        public void Evaluate_Multi_Discount_Multi_X4Y_SomeDiscount()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Multi_Discount_Multi_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Multi_Discount_Multi_X4Y.Id,
                Products = new List<string>()
                {
                    "standout",
                    "standout",
                    "standout"
                }
            });

            AssertResponse(response, 50M);
        }

        [TestMethod]
        public void Evaluate_Multi_Discount_Multi_X4Y_AllDiscount()
        {

            _pricingRuleRepository.FirstOrDefault(Arg.Any<Func<Rule, bool>>()).Returns(Multi_Discount_Multi_X4Y);
            var response = _subject.Evaluate(new PurchasingProduct()
            {
                CustomerType = Multi_Discount_Multi_X4Y.Id,
                Products = new List<string>()
                {
                    "standout",
                    "standout",
                    "standout",
                    "premium",
                    "premium"
                }
            });

            AssertResponse(response, 200M);
        }

        private void AssertResponse(EvaluationResponse response, decimal total)
        {
            Assert.AreEqual(response.Total, total);
        }
    }
}
