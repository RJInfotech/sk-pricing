﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Sk.Pricing.Shared.Models
{
    public class PurchasingProduct
    {
        [JsonProperty("customer")]
        public string CustomerType { get; set; }
        [JsonProperty("products")]
        public ICollection<string> Products { get; set; }
        
    }
}
