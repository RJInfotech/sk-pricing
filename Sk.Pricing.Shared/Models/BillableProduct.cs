﻿namespace Sk.Pricing.Shared.Models
{
    public class BillableProduct
    {
        public string ProductId { get; set; }
        public int Count { get; set; }
        public decimal Cost { get; set; }
    }
}