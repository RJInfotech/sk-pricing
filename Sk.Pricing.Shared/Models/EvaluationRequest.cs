﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Sk.Pricing.Shared.Models
{
    public class EvaluationRequest
    {
        private readonly string _ruleId;

        public EvaluationRequest(ICollection<string> products, string ruleId, Dictionary<string,decimal> defaultRates)
        {
            _ruleId = ruleId;
            Products = products;
            BillableProducts = new List<BillableProduct>();
            foreach (var product in products)
            {
                var pdt = BillableProducts.FirstOrDefault(b => b.ProductId == product);
                if (pdt == null)
                {
                    BillableProducts.Add(new BillableProduct()
                    {
                        ProductId = product,
                        Count = 1,
                        Cost = defaultRates[product]
                    });
                }
                else
                {
                    pdt.Count += 1;
                }
            }
        }

        public ICollection<BillableProduct> BillableProducts { get; }

        public ICollection<string> Products { get; }

        public EvaluationResponse Response
        {
            get
            {
                return new EvaluationResponse()
                {
                    RuleId = _ruleId,
                    Total = BillableProducts.Sum(bp => bp.Count * bp.Cost)
                };
            }
        }
    }

    public class EvaluationResponse
    {
        [JsonProperty("customer")]
        public string RuleId { get; set; }

        [JsonProperty("total")]
        public decimal Total { get; set; }
    }
}