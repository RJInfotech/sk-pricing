﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sk.Pricing.Shared.Enums
{
    public enum AdType
    {
        Classic,
        Standout,
        Premium
    }
}
