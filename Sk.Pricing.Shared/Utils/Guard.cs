﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sk.Pricing.Shared.Utils
{
    public static class Guard
    {
        public static void ArgumentNotNull(object argument, string parameterName = null)
        {
            if (argument == null)
            {
                if (string.IsNullOrEmpty(parameterName))
                {
                    parameterName = nameof(argument);
                }

                throw new ArgumentNullException(parameterName);
            }
        }

        public static void ArgumentNotNullOrEmpty(string argument, string parameterName = null)
        {
            if (string.IsNullOrWhiteSpace(argument))
            {
                if (string.IsNullOrWhiteSpace(parameterName))
                {
                    parameterName = nameof(argument);
                }

                throw new ArgumentNullException(parameterName);
            }
        }
    }
}
