﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Sk.Pricing.Shared.Models;

namespace Sk.Pricing.Shared.ModelValidators
{
    public class PurchasingProductValidator : AbstractValidator<PurchasingProduct>
    {
        public PurchasingProductValidator()
        {
            RuleFor(pp => pp.CustomerType).NotNull().NotEmpty();
            RuleFor(pp => pp.Products).NotNull().WithMessage("Products should be available to checkout");
            RuleFor(pp => pp.Products.Count).GreaterThan(0).WithMessage("Products should be available to checkout");
        }
    }
}
